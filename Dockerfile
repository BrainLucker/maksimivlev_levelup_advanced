FROM markhobson/maven-chrome:jdk-17

LABEL AUTHOR=maksim.ivlev@pprcard.ru
LABEL VERSION=0.1

WORKDIR /app

COPY ./src src/
COPY ./pom.xml pom.xml
COPY ./booking_api_config_prod.json booking_api_config_prod.json
COPY ./banking_project_config_prod.json banking_project_config_prod.json

ENV API_CONFIG_PATH=/app/booking_api_config_prod.json
ENV WEB_UI_CONFIG_PATH=/app/banking_project_config_prod.json
ENV testTypes=ALL

RUN mvn clean package -DskipTests

CMD mvn test -P $testTypes
