package ru.levelp.at.advance.homework1.service.page.element;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

public class TransactionsTableRow {

    private final ElementsCollection cells;

    public TransactionsTableRow(SelenideElement row) {
        cells = row.$$("td");
    }

    public String getDate() {
        return cells.get(0).getText().trim();
    }

    public String getAmount() {
        return cells.get(1).getText().trim();
    }
}
