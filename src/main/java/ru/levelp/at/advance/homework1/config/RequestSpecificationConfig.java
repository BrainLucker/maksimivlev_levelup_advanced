package ru.levelp.at.advance.homework1.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import java.io.File;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RequestSpecificationConfig {

    @SneakyThrows
    public static RequestSpecification defaultSpecification() {
        final String variableName = "API_CONFIG_PATH";
        final String configFilePath = System.getenv(variableName);

        if (configFilePath == null) {
            throw new IllegalArgumentException(
                String.format("Please set up %s variable to environment variables", variableName));
        }

        var apiConfiguration = new ObjectMapper().readValue(new File(configFilePath), ApiConfiguration.class);

        return new RequestSpecBuilder()
            .addFilter(new AllureRestAssured())
            .setBaseUri(apiConfiguration.url())
            .setBasePath(apiConfiguration.basePath())
            .setContentType(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
    }
}
