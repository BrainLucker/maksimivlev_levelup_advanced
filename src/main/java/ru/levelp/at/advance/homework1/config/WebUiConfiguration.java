package ru.levelp.at.advance.homework1.config;

public record WebUiConfiguration(
    String url,
    Boolean headless
) {
}
