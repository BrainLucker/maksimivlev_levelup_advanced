package ru.levelp.at.advance.homework1.config;

public record ApiConfiguration(
    String url,
    String basePath
) {
}
