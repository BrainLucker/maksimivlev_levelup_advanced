package ru.levelp.at.advance.homework1.api.client;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.RequiredArgsConstructor;
import ru.levelp.at.advance.homework1.util.object.BookingInfo;

@RequiredArgsConstructor(staticName = "of")
public class BookingApiClient {

    private static final String BOOKING_ENDPOINT = "/booking";
    private final RequestSpecification requestSpecification;

    public Response createBooking(final BookingInfo bookingInfo) {
        return given()
            .spec(requestSpecification)
            .body(bookingInfo)
            .when()
            .post(BOOKING_ENDPOINT)
            .andReturn();
    }
}
