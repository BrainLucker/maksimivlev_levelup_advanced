package ru.levelp.at.advance.homework1.service.page.object;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.levelp.at.advance.homework1.util.DataGenerator;

public class CustomerLoginPage {
    private final SelenideElement userSelector = $(byId("userSelect"));
    private final ElementsCollection usersList = userSelector.$$("option");
    private final SelenideElement loginButton = $(byAttribute("type", "submit"));

    public CustomerLoginPage clickUserSelector() {
        userSelector.shouldBe(visible).click();
        return this;
    }

    public CustomerLoginPage chooseRandomUser() {
        userSelector.shouldBe(visible).click();
        int i = DataGenerator.generateIntIncluding(1, usersList.size() - 1);
        usersList.get(i).shouldBe(visible).click();
        return this;
    }

    public CustomerLoginPage chooseUser(String userName) {
        userSelector.shouldBe(visible).click();
        usersList.find(exactText(userName)).shouldBe(visible).click();
        return this;
    }

    public CustomerAccountPage clickLoginButton() {
        loginButton.shouldBe(visible).click();
        return new CustomerAccountPage();
    }
}
