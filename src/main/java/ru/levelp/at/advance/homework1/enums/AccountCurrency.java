package ru.levelp.at.advance.homework1.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum AccountCurrency {
    DOLLAR("Dollar"),
    POUND("Pound"),
    RUPEE("Rupee");

    final String name;
}
