package ru.levelp.at.advance.homework1.service.page.object;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import ru.levelp.at.advance.homework1.enums.AccountCurrency;
import ru.levelp.at.advance.homework1.service.page.element.TransactionsTableRow;

public class CustomerAccountPage {

    public static final String DEPOSIT_SUCCESSFUL_MESSAGE = "Deposit Successful";
    public static final String WITHDRAWAL_FAILED_MESSAGE = "Transaction Failed";

    // Account Info
    private final SelenideElement userName = $(withText("Welcome")).$("span");
    private final SelenideElement accountSelector = $(byId("accountSelect"));
    private final ElementsCollection accountsList = accountSelector.$$("option");
    private final SelenideElement accountInfoText = $(withText("Account Number"));
    private final SelenideElement messageText = $(byClassName("error"));

    // Tabs
    private final SelenideElement transactionsTab = $(byAttribute("ng-click^", "transactions"));
    private final SelenideElement depositTab = $(byAttribute("ng-click^", "deposit"));
    private final SelenideElement withdrawalTab = $(byAttribute("ng-click^", "withdrawl"));

    // Deposit Tab
    private final SelenideElement depositForm = $("form[ng-submit^='deposit'");
    private final SelenideElement depositAmountField = depositForm.$("input");
    private final SelenideElement depositSubmitButton = depositForm.$("button");

    // Withdrawal Tab
    private final SelenideElement withdrawalForm = $("form[ng-submit^='withdrawl'");
    private final SelenideElement withdrawalAmountField = withdrawalForm.$("input");
    private final SelenideElement withdrawalSubmitButton = withdrawalForm.$("button");

    // Transactions Tab
    private final SelenideElement transTableHeader = $("table thead");
    private final SelenideElement sortTransByDateButton = transTableHeader.$("a");
    private final String transTableRow = "tbody tr";


    public CustomerAccountPage() {
        userName.shouldBe(visible);
        accountInfoText.shouldBe(visible);
    }

    public CustomerAccountPage goToDepositTab() {
        depositTab.shouldBe(visible).click();
        depositTab.shouldHave(cssClass("btn-primary"));
        depositForm.shouldBe(visible);
        return this;
    }

    public CustomerAccountPage goToWithdrawalTab() {
        withdrawalTab.shouldBe(visible).click();
        withdrawalTab.shouldHave(cssClass("btn-primary"));
        withdrawalForm.shouldBe(visible);
        return this;
    }

    public CustomerAccountPage goToTransactionsTab() {
        Selenide.sleep(300);
        transactionsTab.shouldBe(visible).click();
        transTableHeader.shouldBe(visible);
        return this;
    }

    public CustomerAccountPage chooseAccount(int index) {
        accountSelector.shouldBe(visible).click();
        accountsList.get(index).shouldBe(visible).click();
        return this;
    }

    public String getAccountNumber() {
        return accountInfoText.getText().split(" , ")[0].split(" : ")[1];
    }

    public int getAccountBalance() {
        String balanceText = accountInfoText.getText().split(" , ")[1].split(" : ")[1];
        return Integer.parseInt(balanceText);
    }

    public String getAccountCurrency() {
        return accountInfoText.getText().split(" , ")[2].split(" : ")[1];
    }

    public CustomerAccountPage checkAccountCurrency(AccountCurrency currency) {
        accountInfoText.shouldHave(text(currency.getName()));
        return this;
    }

    public CustomerAccountPage fillDepositAmountField(String amount) {
        depositAmountField.shouldBe(visible).setValue(amount);
        return this;
    }

    public void clickSubmitDepositButton() {
        depositSubmitButton.shouldBe(visible).click();
    }

    public CustomerAccountPage fillWithdrawalAmountField(String amount) {
        withdrawalAmountField.shouldBe(visible).setValue(amount);
        return this;
    }

    public CustomerAccountPage clickSubmitWithdrawalButton() {
        withdrawalSubmitButton.shouldBe(visible).click();
        return this;
    }

    public CustomerAccountPage checkDepositSuccessfulMessage() {
        messageText.shouldBe(visible).shouldHave(text(DEPOSIT_SUCCESSFUL_MESSAGE));
        return this;
    }

    public CustomerAccountPage checkWithdrawalFailedMessage() {
        messageText.shouldBe(visible).shouldHave(text(WITHDRAWAL_FAILED_MESSAGE));
        return this;
    }

    public CustomerAccountPage clickSortByDateButton() {
        sortTransByDateButton.shouldBe(visible).click();
        return this;
    }

    public TransactionsTableRow getTransactionsTableRow(int index) {
        return new TransactionsTableRow($(transTableRow, index));
    }
}
