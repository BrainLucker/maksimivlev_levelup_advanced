package ru.levelp.at.advance.homework1.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class WebUiConfigurationProvider {

    @SneakyThrows
    public static WebUiConfiguration getConfig() {
        final String variableName = "WEB_UI_CONFIG_PATH";
        final String configFilePath = System.getenv(variableName);

        if (configFilePath == null) {
            throw new IllegalArgumentException(
                String.format("Please set up %s variable to environment variables", variableName));
        }

        return new ObjectMapper().readValue(new File(configFilePath), WebUiConfiguration.class);
    }
}
