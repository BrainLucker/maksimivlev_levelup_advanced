package ru.levelp.at.advance.homework1.util.object;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BookingInfo {

    private String firstname;
    private String lastname;
    private Integer totalprice;
    private Boolean depositpaid;
    private Bookingdates bookingdates;
    private String additionalneeds;

    @Getter
    @AllArgsConstructor
    public static class Bookingdates {
        private String checkin;
        private String checkout;
    }
}
