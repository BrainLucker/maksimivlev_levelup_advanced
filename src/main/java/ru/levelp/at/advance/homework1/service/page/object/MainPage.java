package ru.levelp.at.advance.homework1.service.page.object;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;

import com.codeborne.selenide.SelenideElement;

public class MainPage {

    private final SelenideElement customerLoginButton = $(byAttribute("ng-click^", "customer"));
    private final SelenideElement managerLoginButton = $(byAttribute("ng-click^", "manager"));

    public CustomerLoginPage clickCustomerLoginButton() {
        customerLoginButton.shouldBe(visible, ofSeconds(10)).click();
        return new CustomerLoginPage();
    }

    public ManagerAccountPage clickManagerLoginButton() {
        managerLoginButton.shouldBe(visible, ofSeconds(10)).click();
        return new ManagerAccountPage();
    }
}
