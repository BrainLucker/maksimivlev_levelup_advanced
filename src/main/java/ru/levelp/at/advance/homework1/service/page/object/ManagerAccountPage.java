package ru.levelp.at.advance.homework1.service.page.object;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import ru.levelp.at.advance.homework1.service.page.element.HeaderElement;

@Getter
public class ManagerAccountPage {

    HeaderElement header = new HeaderElement();

    // Tabs
    private final SelenideElement addCustomerTab = $(byAttribute("ng-click^", "addCust"));
    private final SelenideElement openAccountTab = $(byAttribute("ng-click^", "openAccount"));

    // Add Customer Tab
    private final SelenideElement addCustomerForm = $("form[ng-submit^='addCustomer'");
    private final SelenideElement firstNameField = addCustomerForm.$(byAttribute("ng-model", "fName"));
    private final SelenideElement lastNameField = addCustomerForm.$(byAttribute("ng-model", "lName"));
    private final SelenideElement postcodeField = addCustomerForm.$(byAttribute("ng-model", "postCd"));
    private final SelenideElement addCustomerButton = addCustomerForm.$("button");

    // Open Account Tab
    private final SelenideElement openAccountForm = $("form[ng-submit^='process'");
    private final SelenideElement customerSelector = openAccountForm.$(byId("userSelect"));
    private final ElementsCollection customersList = customerSelector.$$("option");
    private final SelenideElement currencySelector = openAccountForm.$(byId("currency"));
    private final ElementsCollection currenciesList = currencySelector.$$("option");
    private final SelenideElement processButton = openAccountForm.$("button");

    public ManagerAccountPage goToAddCustomerTab() {
        addCustomerTab.shouldBe(visible).click();
        addCustomerTab.shouldHave(cssClass("btn-primary"));
        addCustomerForm.shouldBe(visible);
        return this;
    }

    public ManagerAccountPage goToOpenAccountTab() {
        openAccountTab.shouldBe(visible).click();
        openAccountTab.shouldHave(cssClass("btn-primary"));
        openAccountForm.shouldBe(visible);
        return this;
    }

    public ManagerAccountPage fillFirstNameField(String amount) {
        firstNameField.shouldBe(visible).setValue(amount);
        return this;
    }

    public ManagerAccountPage fillLastNameField(String amount) {
        lastNameField.shouldBe(visible).setValue(amount);
        return this;
    }

    public ManagerAccountPage fillPostcodeField(String amount) {
        postcodeField.shouldBe(visible).setValue(amount);
        return this;
    }

    public ManagerAccountPage clickAddCustomerButton() {
        addCustomerButton.shouldBe(visible).click();
        return this;
    }

    public ManagerAccountPage chooseCustomer(String customerName) {
        customerSelector.shouldBe(visible).click();
        customersList.find(exactText(customerName)).shouldBe(visible).click();
        return this;
    }

    public ManagerAccountPage chooseCurrency(String currencyName) {
        currencySelector.shouldBe(visible).click();
        currenciesList.find(exactText(currencyName)).shouldBe(visible).click();
        return this;
    }

    public ManagerAccountPage clickProcessButton() {
        processButton.shouldBe(visible).click();
        return this;
    }

    public ManagerAccountPage closeAlert() {
        Selenide.webdriver().driver().switchTo().alert().accept();
        return this;
    }

    public String getCreatedAccountNumber() {
        String text = Selenide.webdriver().driver().switchTo().alert().getText();
        closeAlert();
        return text.split(" :")[1];
    }
}
