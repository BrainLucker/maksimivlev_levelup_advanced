package ru.levelp.at.advance.homework1.util;

import static ru.levelp.at.advance.homework1.util.DateUtil.getDateAfterNow;

import com.github.javafaker.Faker;
import java.util.Random;
import ru.levelp.at.advance.homework1.util.object.BookingInfo;
import ru.levelp.at.advance.homework1.util.object.BookingInfo.Bookingdates;
import ru.levelp.at.advance.homework1.util.object.User;

public class DataGenerator {

    public static int generateInt(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }

    public static int generateIntIncluding(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }

    public static String generateFirstName() {
        return new Faker().name().firstName();
    }

    public static String generateLastName() {
        return new Faker().name().lastName();
    }

    public static String generatePostcode() {
        return new Faker().address().zipCode();
    }

    public static User generateUser() {
        return new User(generateFirstName(), generateLastName(), generatePostcode());
    }

    public static String generateText() {
        return new Faker().beer().name();
    }

    public static BookingInfo generateBooking() {
        return new BookingInfo(
            generateFirstName(),
            generateLastName(),
            generateInt(100, 10000),
            new Random().nextBoolean(),
            new Bookingdates(getDateAfterNow(generateInt(1, 7)), getDateAfterNow(generateInt(10, 30))),
            generateText()
        );
    }
}
