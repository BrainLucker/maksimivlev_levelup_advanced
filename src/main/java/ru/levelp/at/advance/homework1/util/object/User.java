package ru.levelp.at.advance.homework1.util.object;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class User {

    private String firstName;
    private String lastName;
    private String postcode;

    public String getFullName() {
        return String.join(" ", firstName, lastName);
    }
}
