package ru.levelp.at.advance.homework1.service.page.element;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;
import ru.levelp.at.advance.homework1.service.page.object.MainPage;

public class HeaderElement {

    private final SelenideElement header = $(byClassName("mainhdr"));
    private final SelenideElement homeButton = header.$(byClassName("home"));
    private final SelenideElement title = header.$(byClassName("mainHeading"));

    public HeaderElement() {
        title.shouldBe(visible);
    }

    public MainPage clickHomeButton() {
        homeButton.shouldBe(visible).click();
        return new MainPage();
    }
}
