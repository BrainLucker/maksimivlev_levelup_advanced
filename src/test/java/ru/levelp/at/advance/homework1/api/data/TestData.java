package ru.levelp.at.advance.homework1.api.data;

import static org.junit.jupiter.params.provider.Arguments.arguments;
import static ru.levelp.at.advance.homework1.util.DataGenerator.generateBooking;
import static ru.levelp.at.advance.homework1.util.DataGenerator.generateFirstName;
import static ru.levelp.at.advance.homework1.util.DataGenerator.generateLastName;
import static ru.levelp.at.advance.homework1.util.DataGenerator.generateText;
import static ru.levelp.at.advance.homework1.util.DateUtil.getDateAfterNow;
import static ru.levelp.at.advance.homework1.util.DateUtil.getDateBeforeNow;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import ru.levelp.at.advance.homework1.util.object.BookingInfo;
import ru.levelp.at.advance.homework1.util.object.BookingInfo.Bookingdates;

public class TestData {

    public static Stream<Arguments> validBookingData() {
        return Stream.of(
            arguments(
                "Happy Path",
                generateBooking()
            ),
            arguments(
                "Пустые значения в полях с типом String",
                new BookingInfo("", "", 1000, true,
                    new Bookingdates(getDateAfterNow(3), getDateAfterNow(7)), "")
            ),
            arguments(
                "Даты начала и окончания букинга позже текущей даты",
                new BookingInfo(generateFirstName(), generateLastName(), 111, true,
                    new Bookingdates(getDateAfterNow(5), getDateAfterNow(10)), generateText())
            )
        );
    }

    public static Stream<Arguments> invalidBookingData() {
        return Stream.of(
            arguments(
                "Значение Null во всех полях",
                new BookingInfo(null, null, null, null,
                    new Bookingdates(null, null), null)
            ),
            arguments(
                "Дата начала букинга раньше текущей даты",
                new BookingInfo(generateFirstName(), generateLastName(), 222, true,
                    new Bookingdates(getDateBeforeNow(5), getDateAfterNow(10)), generateText())
            ),
            arguments(
                "Дата окончания букинга раньше текущей даты",
                new BookingInfo(generateFirstName(), generateLastName(), 333, true,
                    new Bookingdates(getDateAfterNow(5), getDateBeforeNow(10)), generateText())
            ),
            arguments(
                "Дата окончания букинга раньше даты его начала",
                new BookingInfo(generateFirstName(), generateLastName(), 444, true,
                    new Bookingdates(getDateAfterNow(10), getDateAfterNow(5)), generateText())
            )
        );
    }
}
