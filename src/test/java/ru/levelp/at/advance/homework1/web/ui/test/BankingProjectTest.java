package ru.levelp.at.advance.homework1.web.ui.test;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.Assertions.assertThat;
import static ru.levelp.at.advance.homework1.util.DateUtil.formatDate;

import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.levelp.at.advance.homework1.enums.AccountCurrency;
import ru.levelp.at.advance.homework1.service.page.object.CustomerAccountPage;
import ru.levelp.at.advance.homework1.service.page.object.MainPage;
import ru.levelp.at.advance.homework1.util.DataGenerator;

@Tag("ui")
@DisplayName("UI тесты банковского приложения")
public class BankingProjectTest extends AbstractBaseTest {

    @Test
    @DisplayName("Пополнение счета")
    void testAccountFunding() {
        final var amountToDeposit = DataGenerator.generateInt(150, 2500);

        var customerAccountPage = step("Вход в аккаунт случайного клиента и выбор счета в рупиях", () ->
            new MainPage().clickCustomerLoginButton()
                          .clickUserSelector()
                          .chooseRandomUser()
                          .clickLoginButton()
                          .chooseAccount(2)
                          .checkAccountCurrency(AccountCurrency.RUPEE)
        );

        final var expectedDate = step("Пополнение выбранного счета на случайную сумму", () -> {
                final var balanceBefore = customerAccountPage.getAccountBalance();
                customerAccountPage.goToDepositTab()
                                   .fillDepositAmountField(String.valueOf(amountToDeposit))
                                   .clickSubmitDepositButton();
                final var transactionTime = LocalDateTime.now();
                final var balanceAfter = customerAccountPage.checkDepositSuccessfulMessage()
                                                            .getAccountBalance();

                assertThat(balanceBefore + amountToDeposit)
                    .as("Проверка изменения баланса счета на сумму пополнения")
                    .isEqualTo(balanceAfter);
                return transactionTime;
            }
        );

        step("Открытие совершенной транзакции на табе Transactions", () -> {
                var transaction = new CustomerAccountPage().goToTransactionsTab()
                                                           .clickSortByDateButton()
                                                           .getTransactionsTableRow(0);
                final var dateFormat = "MMM d, yyyy h:mm:ss a";

                assertThat(formatDate(expectedDate, dateFormat))
                    .as("Проверка даты и времени пополнения")
                    .contains(transaction.getDate());
                assertThat(String.valueOf(amountToDeposit))
                    .as("Проверка суммы пополнения")
                    .contains(transaction.getAmount());
            }
        );
    }

    @Test
    @DisplayName("Открытие нового счёта для нового клиента")
    void shouldCreateNewCustomerAccount() {
        var user = DataGenerator.generateUser();
        final var expectedCurrency = AccountCurrency.POUND.getName();

        var managerAccountPage = step("Вход в аккаунт менеджера и создание нового клиента", () ->
            new MainPage().clickManagerLoginButton()
                          .goToAddCustomerTab()
                          .fillFirstNameField(user.getFirstName())
                          .fillLastNameField(user.getLastName())
                          .fillPostcodeField(user.getPostcode())
                          .clickAddCustomerButton()
                          .closeAlert()
        );

        final var expectedAccountNumber = step("Открытие счета для созданного клиента, получение его номера", () ->
            managerAccountPage.goToAddCustomerTab()
                              .goToOpenAccountTab()
                              .chooseCustomer(user.getFullName())
                              .chooseCurrency(expectedCurrency)
                              .clickProcessButton()
                              .getCreatedAccountNumber()
        );

        step("Вход в аккаунт созданного клиента", () -> {
                var customerAccountPage = managerAccountPage.getHeader().clickHomeButton()
                                                            .clickCustomerLoginButton()
                                                            .clickUserSelector()
                                                            .chooseUser(user.getFullName())
                                                            .clickLoginButton();

                assertThat(expectedAccountNumber)
                    .as("Проверка номера созданного аккаунта")
                    .isEqualTo(customerAccountPage.getAccountNumber());
                assertThat(expectedCurrency)
                    .as("Проверка валюты созданного аккаунта")
                    .isEqualTo(customerAccountPage.getAccountCurrency());
            }
        );
    }

    @Test
    @DisplayName("Снятие со счета недопустимой суммы")
    void testIneligibleWithdraw() {
        var customerAccountPage = step("Вход в аккаунт случайного клиента и выбор счета в долларах", () ->
            new MainPage().clickCustomerLoginButton()
                          .clickUserSelector()
                          .chooseRandomUser()
                          .clickLoginButton()
                          .chooseAccount(0)
                          .checkAccountCurrency(AccountCurrency.DOLLAR)
        );

        step("Проверка отображения ошибки при снятии со счета недопустимой суммы", () -> {
                final var balanceBefore = customerAccountPage.getAccountBalance();
                final var amountToWithdraw = String.valueOf(balanceBefore + DataGenerator.generateInt(1, 500));

                var balanceAfter = customerAccountPage.goToWithdrawalTab()
                                                      .fillWithdrawalAmountField(amountToWithdraw)
                                                      .clickSubmitWithdrawalButton()
                                                      .checkWithdrawalFailedMessage()
                                                      .getAccountBalance();

                assertThat(balanceBefore)
                    .as("Проверка, что баланс счета не изменился")
                    .isEqualTo(balanceAfter);
            }
        );
    }
}
