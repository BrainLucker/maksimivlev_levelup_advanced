package ru.levelp.at.advance.homework1.api.test;

import static io.qameta.allure.Allure.step;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.levelp.at.advance.homework1.api.client.BookingApiClient;
import ru.levelp.at.advance.homework1.config.RequestSpecificationConfig;
import ru.levelp.at.advance.homework1.util.object.BookingInfo;

@Tag("api")
@DisplayName("API тесты сервиса бронирования")
public class BookerTest {

    @ParameterizedTest(name = " : {0}")
    @DisplayName("Создание букинга с валидными данными")
    @MethodSource("ru.levelp.at.advance.homework1.api.data.TestData#validBookingData")
    void testCreateValidBooking(String testName, BookingInfo bookingInfo) {
        final var response = step("Отправка запроса", () ->
            BookingApiClient.of(RequestSpecificationConfig.defaultSpecification()).createBooking(bookingInfo))
            .then()
            .log().all()
            .statusCode(SC_OK);

        step("Проверка id созданного бронирования", () ->
            response.body("bookingid", Matchers.notNullValue())
        );

        step("Проверка данных созданного бронирования", () ->
            response.rootPath("booking")
                    .body("firstname", equalTo(bookingInfo.getFirstname()))
                    .body("lastname", equalTo(bookingInfo.getLastname()))
                    .body("totalprice", equalTo(bookingInfo.getTotalprice()))
                    .body("depositpaid", equalTo(bookingInfo.getDepositpaid()))
                    .body("bookingdates.checkin", equalTo(bookingInfo.getBookingdates().getCheckin()))
                    .body("bookingdates.checkout", equalTo(bookingInfo.getBookingdates().getCheckout()))
                    .body("additionalneeds", equalTo(bookingInfo.getAdditionalneeds()))
        );
    }

    @ParameterizedTest(name = " : {0}")
    @DisplayName("Создание букинга с невалидными данными")
    @MethodSource("ru.levelp.at.advance.homework1.api.data.TestData#invalidBookingData")
    void testCreateInvalidBooking(String testName, BookingInfo bookingInfo) {
        step("Отправка запроса", () ->
            BookingApiClient.of(RequestSpecificationConfig.defaultSpecification()).createBooking(bookingInfo))
            .then()
            .log().all()
            .statusCode(SC_INTERNAL_SERVER_ERROR);
    }
}
