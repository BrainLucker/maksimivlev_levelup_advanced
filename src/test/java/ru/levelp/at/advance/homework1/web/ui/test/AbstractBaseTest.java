package ru.levelp.at.advance.homework1.web.ui.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import ru.levelp.at.advance.homework1.config.WebUiConfiguration;
import ru.levelp.at.advance.homework1.config.WebUiConfigurationProvider;

public abstract class AbstractBaseTest {

    protected WebUiConfiguration config;

    @BeforeEach
    void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        config = WebUiConfigurationProvider.getConfig();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");

        Configuration.browserCapabilities = new DesiredCapabilities();
        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        Configuration.timeout = 10000;
        Configuration.headless = config.headless();

        Selenide.open(config.url());
    }

    @AfterEach
    void tearDown() {
        Selenide.closeWebDriver();
    }
}
